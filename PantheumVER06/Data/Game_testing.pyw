import traceback
from tkinter import *
from tkinter import messagebox
from random import randint, choice
from threading import Thread
from math import ceil
import pygame
import pyglet
import json
import os


try:
    if os.getcwd()[-4:] == 'Data':  # запуск с скрипта
        dir = os.getcwd()
    else:  # запуск с лаунчера
        dir = os.getcwd() + '/Data'

    pyglet.font.add_file(f'{dir}/font.ttf')
    file = json.load(open(f'{dir}/data_file.json'))['Player']
except FileNotFoundError:
    print(traceback.format_exc())
    messagebox.showerror('Error!', 'Сохраненные данные не найдены')
    sys.exit()


def music_thread():
    """Музыка"""
    import pygame

    pygame.init()

    pygame.mixer.music.load(f'{dir}/snd/game_music.mp3')
    pygame.mixer.music.play(-1)
    """======"""


def draw_stat():
    stat = ((PlayerG.dmg_points, (20, 110)),
            (PlayerG.hp_points, (155, 245)),
            (PlayerG.crit_points, (290, 380)))

    for i in range(len(stat)):
        y1, y2 = 450, 480
        for j in range(stat[i][0] + PlayerG().class_player.stats_up[i]):
            canvas.create_rectangle(stat[i][1][0], y1, stat[i][1][1], y2, fill='purple4', tag='up_ui')
            y1 -= 45
            y2 -= 45


class PlayerG:
    name = file['Name']
    max_hp = file['MAX_HP']
    hp = file['HP'][0]
    hp_points = file['HP'][1]
    level = file['Level']
    xp = file['XP']
    sp = file['SP']
    max_xp = file['MAX_XP']
    damage = file['Damage'][0]
    dmg_points = file['Damage'][1]
    critical_damage = file['CRITICAL'][0]
    crit_points = file['CRITICAL'][1]
    dungeon = file['DUNGEON']
    dungeon_level = 1

    def __init__(self):
        c = {'Secateurs': Secateurs, 'Reactive': Reactive, 'Celestial': Celestial}
        self.class_player = c.get(file['Class'])

    def save_data(self):
        player_profile = {'Player': {
            'Name': self.name,
            'Class': self.class_player.class_name,
            'Level': self.level,
            'DUNGEON': self.dungeon,
            'XP': self.xp,
            'MAX_XP': self.max_xp,
            'SP': self.sp,
            'HP': [self.hp, self.hp_points],
            'MAX_HP': self.max_hp,
            'Damage': [self.damage, self.dmg_points],
            'CRITICAL': [self.critical_damage, self.crit_points]
        }}

        json.dump(player_profile, open(f'{dir}/data_file.json', 'w'), indent=4)
        print('Data saved!')

    def check_sp(self):
        points = ((self.dmg_points, 0, sk1), (self.hp_points, 1, sk2), (self.crit_points, 2, sk3))
        for i in range(len(points)):
            if points[i][0] + self.class_player.stats_up[points[i][1]] < 10 and self.sp > 0:
                points[i][2].config(state='normal')
            else:
                points[i][2].config(state='disabled')

    def stat_up(self, stat):
        PlayerG.sp -= 1

        if stat == 'dmg':
            PlayerG.damage = ceil(self.damage * 1.5)
            PlayerG.dmg_points += 1

        elif stat == 'hp':
            PlayerG.max_hp = ceil(self.max_hp * 1.5)
            PlayerG.hp_points += 1

        elif stat == 'crit':
            PlayerG.critical_damage = self.critical_damage + 0.2
            PlayerG.crit_points += 1

        draw_stat()

        PlayerG().check_sp()
        PlayerG().save_data()


class Secateurs(PlayerG):
    class_name = 'Secateurs'
    stats_up = [2, 0, 0]


class Reactive(PlayerG):
    class_name = 'Reactive'
    stats_up = [0, 0, 2]


class Celestial(PlayerG):
    class_name = 'Celestial'
    stats_up = [0, 2, 0]


def start_level(x, lvl, w, restart):
    window.unbind('<MouseWheel>')
    if PlayerG.dungeon_level == 10 or restart:
        canvas.delete('win')
        canvas.itemconfig(btn_right, fill='white')
        PlayerG.dungeon_level = 1
        canvas.itemconfig(lbl_dungeon_level, text=f'Level {PlayerG.dungeon_level}')
    PlayerG.dungeon = lvl
    for i in range(40):
        x -= 20
        w += 5
        window.after(1, window.geometry(f'{x}x600+{w}+200'))
        window.update()

    levels_visible(False)
    canvas.itemconfig('ui', state='normal')
    canvas.itemconfig('play_ui', state='hidden')
    canvas.itemconfig('map_ui', state='hidden')
    canvas.itemconfig('BETA', state='hidden')

    for i in range(20):
        x += 20
        w -= 5
        window.after(1, window.geometry(f'{x}x600+{w}+200'))
        window.update()

    if PlayerG.sp <= 0:
        canvas.itemconfig(lbl_skill_points, text='')
    else:
        canvas.itemconfig(lbl_skill_points, text='+')

    PlayerG().save_data()
    window.bind('<MouseWheel>', lambda self: upgrade(400, 700))
    window.bind('<Button-3>', lambda self: generate_enemy())


def upgrade(x, w):
    print('Очки:', PlayerG.sp)
    window.unbind('<MouseWheel>')
    window.unbind('<Button-3>')

    for i in range(20):
        x -= 20
        w += 5
        window.after(1, window.geometry(f'{x}x600+{w}+200'))
        window.update()

    canvas.itemconfig('ui', state='hidden')
    canvas.itemconfig('win', state='hidden')
    canvas.itemconfig('BETA', state='normal')
    canvas.itemconfig('up_ui', state='normal')

    sk1.place(x=30, y=510)
    sk2.place(x=165, y=510)
    sk3.place(x=300, y=510)

    draw_stat()

    PlayerG().check_sp()

    for i in range(20):
        x += 20
        w -= 5
        window.after(1, window.geometry(f'{x}x600+{w}+200'))
        window.update()

    window.bind('<MouseWheel>', lambda self: level_map(400, 700))


def level_map(x, w):
    window.unbind('<MouseWheel>')
    window.unbind('<Button-3>')
    canvas.itemconfig('up_ui', state='hidden')
    canvas.itemconfig('map_ui', state='normal')
    sk1.place_forget()
    sk2.place_forget()
    sk3.place_forget()
    PlayerG.y1, PlayerG.y2 = 490, 510

    canvas.config(width=800, height=615)

    levels_visible(True)

    for i in range(20):
        x += 20
        w -= 10
        window.after(5, window.geometry(f'{x}x600+{w}+200'))
        window.update()

    PlayerG().save_data()
    window.bind('<MouseWheel>', lambda self: start_level(800, PlayerG.dungeon, 615, False))


def generate_attack_line():
    Enemy.x1, Enemy.x2 = randint(100, 300), randint(100, 300)
    while Enemy.x2 - Enemy.x1 > 40 or Enemy.x2 - Enemy.x1 < 15:
        Enemy.x1, Enemy.x2 = randint(100, 300), randint(100, 300)
    canvas.coords(attack_line2, Enemy.x1, 560, Enemy.x2, 560)
    update()


def state_attack_button(state):
    if state:
        window.bind('<Button-1>', lambda self: attack())
    else:
        window.unbind('<Button-1>')


def attack():
    canvas.itemconfig(btn_left, fill='grey')
    state_attack_button(0)
    if Enemy.x1 <= canvas.coords(attack_line3)[2] <= Enemy.x2 and canvas.coords(attack_cooldown)[2] == 150:
        Enemy.enemy_hp -= randint(PlayerG.damage, ceil(PlayerG.damage + (PlayerG.damage * PlayerG.critical_damage)))
        damage_animation(-3)
        check()
        generate_attack_line()
    else:
        enemy_attack()
        canvas.itemconfig(btn_left, fill='white')
        state_attack_button(1)
        generate_attack_line()


def check():
    if Enemy.enemy_hp <= 0:
        PlayerG.xp += Enemy.xp
        canvas.delete('enemy')
        canvas.itemconfig('line', state='hidden')
        canvas.itemconfig(lbl_enemy_hp, state='hidden')
        canvas.itemconfig(lbl_player_hp_xp, text=f'[HP {PlayerG.hp}]/[XP {PlayerG.xp}/{PlayerG.max_xp}]')
        canvas.itemconfig(btn_right, fill='white')
        window.bind('<Button-3>', lambda self: generate_enemy())
        change_level()
        window.bind('<MouseWheel>', lambda self: upgrade(400, 700))
        PlayerG().save_data()
    elif Enemy.enemy_hp > 0:
        canvas.itemconfig(lbl_enemy_hp, text=f'Polygon [ {Enemy.enemy_hp} ]')
        cooldown()


def cooldown():
    canvas.itemconfig(attack_cooldown, state='normal')
    x = canvas.coords(attack_cooldown)[2]
    canvas.itemconfig(btn_left, fill='grey')
    state_attack_button(0)
    canvas.itemconfig(attack_line3, state='hidden')
    damage = False
    damage_time = 0
    if randint(0, 100) < Enemy.attack_chance:
        damage_time = randint(0, 100)
        damage = True
    for i in range(100):
        if damage and i == damage_time:
            enemy_attack()
        x += 1
        canvas.after(10, canvas.coords(attack_cooldown, 150, 590, x, 590))
        canvas.update()
    canvas.coords(attack_cooldown, 150, 590, 150, 590)
    canvas.itemconfig(attack_line3, state='normal')
    canvas.itemconfig(btn_left, fill='white')
    state_attack_button(1)


def change_level():
    if PlayerG.dungeon_level < 10:
        PlayerG.dungeon_level += 1
        canvas.itemconfig(lbl_dungeon_level, text=f'Level {PlayerG.dungeon_level}')
    else:
        canvas.create_text(200, 220, text='You\ncomplete\nthis\ndungeon!', font='Plateia 15', fill='white', tag='win')
        PlayerG.hp = PlayerG.max_hp
        canvas.itemconfig(lbl_player_hp_xp, text=f'[HP {PlayerG.hp}]/[XP {PlayerG.xp}/{PlayerG.max_xp}]')
        canvas.itemconfig('control', fill='grey')
        state_attack_button(0)
        window.unbind('<Button-3>')


stop = False


def update():
    global stop
    direction = 3
    while not stop:
        if PlayerG.xp >= PlayerG.max_xp:
            PlayerG.xp -= PlayerG.max_xp
            PlayerG.max_xp = ceil(PlayerG.max_xp * 1.5)
            PlayerG.level += 1
            PlayerG.sp += 1
            canvas.itemconfig(lbl_skill_points, text='+')
            PlayerG.hp = PlayerG.max_hp
            canvas.itemconfig(lbl_player_name, text=f'[ {PlayerG.level} ] {PlayerG.name}')
            canvas.itemconfig(lbl_player_hp_xp, text=f'[HP {PlayerG.hp}]/[XP {PlayerG.xp}/{PlayerG.max_xp}]')
        if Enemy.enemy_hp <= 0:
            break
        if PlayerG.hp <= 0:
            game_over()
            break
        window.after(10, canvas.move(attack_line3, direction, 0))
        if canvas.coords(attack_line3)[2] >= 300:
            direction = -3
        if canvas.coords(attack_line3)[2] < 100:
            direction = 3
        canvas.update()


class Enemy:
    def __init__(self, color):
        self.class_enemy = color

    enemy_hp = 0
    damage = 0
    attack_chance = 0
    xp = 0
    x1, x2 = 0, 0


class White(Enemy):
    color = 'white'
    stats = [5, 5, 1, 30]  # hp/dmg/xp/%attack chance


class Green(Enemy):
    color = 'green'
    stats = [randint(6, 11), randint(5, 8), 2, 40]


class Blue(Enemy):
    color = 'blue'
    stats = [randint(7, 12), randint(6, 9), 3, 50]


class Purple(Enemy):
    color = 'purple'
    stats = [randint(8, 13), randint(7, 12), 4, 60]


class Orange(Enemy):
    color = 'orange'
    stats = [randint(9, 14), randint(10, 15), 5, 70]


def generate_enemy():
    window.unbind('<MouseWheel>')
    PlayerG().save_data()
    canvas.itemconfig(btn_left, fill='white')
    state_attack_button(1)
    canvas.itemconfig(btn_right, fill='grey')
    window.unbind('<Button-3>')
    canvas.itemconfig('line', state='normal')
    canvas.delete('enemy')
    enemy_colors = choice((White, Green, Blue, Purple, Orange))
    enm = Enemy(enemy_colors).class_enemy
    Enemy.enemy_hp = enm.stats[0] * PlayerG.dungeon
    Enemy.damage = enm.stats[1] * PlayerG.dungeon
    Enemy.xp = enm.stats[2] * PlayerG.dungeon
    Enemy.attack_chance = enm.stats[3]
    canvas.itemconfig(lbl_enemy_hp, text=f'Polygon [ {Enemy.enemy_hp} ]', state='normal')
    rnd = randint(20, 100)
    if rnd % 2 != 0:
        rnd += 1
    enemy_points = []
    for i in range(rnd):
        if i % 2 != 0:
            enemy_points.append(randint(130, 460))
        else:
            enemy_points.append(randint(130, 270))
    canvas.create_polygon(enemy_points, fill=enemy_colors.color, tag='enemy')

    generate_attack_line()


def enemy_attack():
    PlayerG.hp -= Enemy.damage
    canvas.itemconfig(lbl_player_hp_xp, text=f'[HP {PlayerG.hp}]/[XP {PlayerG.xp}/{PlayerG.max_xp}]')
    canvas['bg'] = 'red'
    canvas.itemconfig(lbl_bg, image=bg_red)
    canvas.update()
    window.after(100)
    canvas['bg'] = 'black'
    canvas.itemconfig(lbl_bg, image=bg)


def damage_animation(direction):
    first_x = canvas.coords('enemy')[2]
    cnt = 0
    while cnt != 4:
        window.after(0, canvas.move('enemy', direction, 0))
        canvas.update()
        if canvas.coords('enemy')[2] < first_x - 100 and cnt < 3:
            direction = 1
            cnt += 1
        if canvas.coords('enemy')[2] > first_x + 100 and cnt < 3:
            direction = -1
            cnt += 1
        if canvas.coords('enemy')[2] == first_x and cnt == 3:
            cnt += 1
    canvas.update()


def game_over():
    global stop
    stop = True
    state_attack_button(0)
    canvas.delete('all')

    pygame.mixer.music.load(f"{dir}/snd/Game_over3.wav")
    pygame.mixer.music.play()
    for _ in range(200):
        canvas.delete('all')
        window.after(10)
        canvas.update()
        rnd = randint(20, 40)
        if rnd % 2 != 0:
            rnd += 1
        points = []
        for i in range(rnd):
            if i % 2 != 0:
                points.append(randint(0, 600))
            else:
                points.append(randint(0, 400))
        canvas.create_polygon(points, fill='red', tag='pol')
        canvas.update()

    gm = canvas.create_text(200, 280, text='', fill='white', font='Plateia 40')
    esc = canvas.create_text(200, 560, text='', fill='white', font='Plateia 13')
    text = ('You\n died', '[ESC]')

    for i in range(len(text[0]) + 1):
        canvas.itemconfig(gm, text=text[0][0:i])
        canvas.update()
        window.after(200)

    for i in range(len(text[1]) + 1):
        canvas.itemconfig(esc, text=text[1][0:i])
        canvas.update()
        window.after(200)


window = Tk()
window.grab_set()
window.geometry('400x600+700+200')
window['bg'] = 'black'
window.resizable(False, False)
window.title('VER.TESTING')
canvas = Canvas(window, width=400, height=600, bg='black', highlightthickness=0)
canvas.pack()


def first_resize(x, y, w):
    window.overrideredirect(True)
    for i in range(40):
        x += i
        w -= i - 16
        window.after(5, window.geometry(f'{x}x{y}+{w}+200'))
        window.update()
    for i in range(29):
        y += i + 6
        window.after(5, window.geometry(f'{x}x{y}+{w}+200'))
        window.update()


def levels_visible(n):
    if n:
        coords = ((150, 140), (440, 110), (690, 190), (540, 330), (290, 480))
        [levels[level].place(x=coords[level][0], y=coords[level][1]) for level in range(len(levels))]
    else:
        [level.place_forget() for level in levels]


if __name__ == '__main__':
    Thread(target=music_thread).start()
    first_resize(20, 20, 700)
    bg = PhotoImage(file=f'{dir}/img/play_bg.png')
    lbl_bg = canvas.create_image(200, 300, image=bg, tag='ui')
    bg_red = PhotoImage(file=f'{dir}/img/play_bg_red.png')

    btn_left = canvas.create_polygon(0, 500, 80, 530, 80, 600, 0, 600,
                                     fill='grey',
                                     tag=['control', 'ui'])
    btn_right = canvas.create_polygon(320, 530, 400, 500, 400, 600, 320, 600,
                                      fill='white',
                                      tag=['control', 'ui'])

    attack_cooldown = canvas.create_line(150, 590, 150, 590,
                                         fill='white',
                                         width=5)

    attack_line = canvas.create_line(100, 560, 300, 560,
                                     fill='white',
                                     width=4,
                                     tag=['line', 'play_ui'])
    attack_line3 = canvas.create_line(100, 540, 100, 580,
                                      fill='white',
                                      width=5,
                                      tag=['line', 'play_ui'])
    attack_line2 = canvas.create_line(Enemy.x1, 560, Enemy.x2, 560,
                                      fill='white',
                                      width=12,
                                      tag=['line', 'play_ui'])

    lbl_player_name = canvas.create_text(2, 18,
                                         text=f'[ {PlayerG.level} ] {PlayerG.name}',
                                         font='Plateia 15',
                                         fill='white',
                                         anchor=W,
                                         tag='ui')

    canvas.create_line(0, 40, 200, 40,
                       width=3,
                       fill='white', tag='ui')
    lbl_player_hp_xp = canvas.create_text(2, 50,
                                          text=f'[HP {PlayerG.hp}]/[XP {PlayerG.xp}/{PlayerG.max_xp}]',
                                          font='Plateia 10',
                                          fill='white',
                                          anchor=W,
                                          tag='ui')

    lbl_sp_frame = canvas.create_line(350, 10, 390, 10, 390, 40,
                                      width=3,
                                      fill='white',
                                      tag='ui')
    lbl_skill_points = canvas.create_text(370, 20,
                                          text='',
                                          fill='white',
                                          font='Plateia 15',
                                          tag=['sp', 'ui'])

    lbl_dungeon_level = canvas.create_text(160, 111,
                                           text=f'Level {PlayerG.dungeon_level}',
                                           font='Plateia 10',
                                           fill='black',
                                           tag='ui')
    lbl_enemy_hp = canvas.create_text(200, 489,
                                      text='',
                                      font='Plateia 10',
                                      fill='black',
                                      tag='play_ui')
    up_bg = PhotoImage(file=f'{dir}/img/upgrade.png')
    canvas.create_image(200, 300, image=up_bg, tag='up_ui')
    sk1 = Button(window,
                 text='DMG',
                 width=5,
                 relief='groove',
                 bg='purple4',
                 fg='white',
                 font='Plateia 10',
                 command=lambda: PlayerG().stat_up('dmg'))
    sk2 = Button(window, text='HP',
                 width=5,
                 relief='groove',
                 bg='purple4',
                 fg='white',
                 font='Plateia 10',
                 command=lambda: PlayerG().stat_up('hp'))
    sk3 = Button(window,
                 text='CRIT',
                 width=5,
                 relief='groove',
                 bg='purple4',
                 fg='white',
                 font='Plateia 10',
                 command=lambda: PlayerG().stat_up('crit'))
    canvas.create_text(65, 560,
                       text='',
                       fill='white',
                       font='Plateia 10',
                       tag='up_ui')
    canvas.create_text(200, 560,
                       text='',
                       fill='white',
                       font='Plateia 10',
                       tag='up_ui')
    canvas.create_text(335, 560,
                       text='',
                       fill='white',
                       font='Plateia 10',
                       tag='up_ui')

    map_img = PhotoImage(file=f'{dir}/img/map_img.png')
    canvas.create_image(0, 0, image=map_img, tag='map_ui', anchor=NW, state='hidden')

    canvas.create_text(400, 20,
                       text='MAP',
                       fill='white',
                       font='Plateia 20',
                       tag='map_ui')

    levels = []
    for level in range(5):
        levels.append(Button(window, text=level+1, width=2, relief='flat', bg='white', font='Plateia 10',
                      command=lambda: start_level(800, level+1, 615, True)))

    level_line = canvas.create_line((150, 160),
                                    (450, 130),
                                    (710, 200),
                                    (550, 350),
                                    (300, 500),
                                    width=5,
                                    fill='white',
                                    tag='map_ui')
    canvas.itemconfig('all', state='hidden')
    canvas.config(width=800, height=600)

    canvas.create_text(25, 590,
                       text='TESTING',
                       fill='white',
                       tag='BETA')

    canvas.itemconfig('map_ui', state='normal')
    levels_visible(True)
    window.bind('<Escape>', lambda self: window.destroy())
    window.mainloop()
