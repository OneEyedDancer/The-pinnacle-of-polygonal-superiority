from tkinter import *
import subprocess
from threading import Thread
from random import randint
from tkinter import messagebox
from os import path
import plyer
import pyglet
import json
import sys

window = Tk()
window.geometry('800x300+550+300')
window.resizable(False, False)
window.overrideredirect(True)
window.title('')
window.iconbitmap('Data/img/icon.ico')
window['bg'] = 'black'
canvas = Canvas(window, width=800, height=300, bg='black', highlightthickness=0)
canvas.pack()

pyglet.font.add_file('Data/font.ttf')

bg = PhotoImage(file='Data/img/menu_bg.png')
logo = PhotoImage(file='Data/img/logo1.png').subsample(8, 8)
canvas.create_image(400, 150, image=logo, tag='Reactive')
canvas.create_line(0, 290, 800, 290, fill='white', width='20')
canvas.create_text(740, 290, text='Starting...', font='Plateia 10')


def music_thread():
    """Музыка"""
    import pygame

    pygame.init()

    pygame.mixer.music.load('Data/snd/menu_music.wav')
    pygame.mixer.music.play(-1)

    global stop
    while not stop:
        pass
    pygame.mixer.quit()
    """======"""


stop = False  # глобальная переменная для остановки потока
thr = Thread(target=music_thread)


def start_logo(y, h):
    for i in range(25):
        y -= i
        window.after(5, window.geometry(f'800x{y}+550+{h}'))
        window.update()
    canvas.delete('all')
    main_menu()
    for i in range(25):
        y += i
        window.after(5, window.geometry(f'800x{y}+550+{h}'))
        window.update()
    thr.start()


window.after(2000, lambda: start_logo(300, 300))


def main_menu():
    def start_new_game():
        window.overrideredirect(False)
        window.iconify()
        class_window = Toplevel(window)
        class_window.geometry('700x300+600+300')
        class_window.overrideredirect(True)
        class_window.title('SELECT')
        class_window['bg'] = 'black'
        canvas2 = Canvas(class_window, width=700, height=300, bg='black', highlightthickness=0)
        canvas2.pack()

        canvas2.create_text(350, 20,
                            text='SELECT CLASS',
                            font='Plateia 15',
                            fill='white')

        rect1 = canvas2.create_polygon((80, 50), (200, 50), (200, 250), (80, 250), fill='grey', activefill='white',
                                       tag='Secateurs')
        img1 = PhotoImage(file='Data/img/Secateurs.png').subsample(10, 10)
        canvas2.create_image(140, 150, image=img1, tag=['Secateurs', 'img1'])
        canvas2.create_text(140, 270,
                            text='Secateurs',
                            font='Plateia 10',
                            fill='white')

        rect2 = canvas2.create_polygon((290, 50), (410, 50), (410, 250), (290, 250), fill='grey', activefill='white',
                                       tag='Reactive')
        img2 = PhotoImage(file='Data/img/Reactive.png').subsample(12, 12)
        canvas2.create_image(350, 150, image=img2, tag=['Reactive', 'img2'])
        canvas2.create_text(350, 270,
                            text='Reactive',
                            font='Plateia 10',
                            fill='white')

        rect3 = canvas2.create_polygon((500, 50), (620, 50), (620, 250), (500, 250), fill='grey', activefill='white',
                                       tag='Celestial')
        img3 = PhotoImage(file='Data/img/Сelestial.png').subsample(12, 12)
        canvas2.create_image(560, 150, image=img3, tag=['Celestial', 'img3'])
        canvas2.create_text(560, 270,
                            text='Сelestial',
                            font='Plateia 10',
                            fill='white')

        def highlighting(event, rect):
            canvas2.itemconfig(rect, fill='white')

        def off_highlighting(event, rect):
            canvas2.itemconfig(rect, fill='grey')

        rects = [rect1, rect2, rect3]
        for b in range(1, 4):
            canvas2.tag_bind(f'img{b}', '<Enter>', lambda event, arg=rects[b - 1]: highlighting(event, arg))
            canvas2.tag_bind(f'img{b}', '<Leave>', lambda event, arg=rects[b - 1]: off_highlighting(event, arg))

        def entry_name(class_choice):
            canvas2.delete('all')
            canvas2.create_text(350, 120, text='Enter character name', font='Plateia 15', fill='white')

            n = StringVar()

            entry = Entry(class_window,
                          textvariable=n,
                          bg='white',
                          fg='black',
                          relief='flat',
                          width=15,
                          font='Plateia 10'
                          )
            entry.focus_set()
            entry.place(x=260, y=150)

            def save_new_character():
                if len(n.get()) > 10:
                    messagebox.showinfo('Недопустимые значения', 'Не больше 10 символов!')
                    return
                if n.get() == '':
                    name = 'Unknown'
                else:
                    name = n.get()

                player_profile = {'Player': {
                    'Name': name,
                    'Class': class_choice,
                    'Level': 0,
                    'DUNGEON': 0,
                    'XP': 0,
                    'MAX_XP': 5,
                    'SP': 0,
                    'HP': [100, 0],
                    'MAX_HP': 100,
                    'Damage': [1, 0],
                    'CRITICAL': [0.2, 0]
                }}

                json.dump(player_profile, open('Data/data_file.json', 'w'), indent=4)
                plyer.notification.notify(message=f'Good luck, {n.get()}!', timeout=10)
                if path.isfile('Data/Game_testing.pyw'):
                    subprocess.Popen([sys.executable, 'Data/Game_testing.pyw'])
                else:
                    subprocess.Popen('Data/Game_testing.exe')
                window.overrideredirect(False)
                window.iconify()
                class_window.destroy()
                global stop
                stop = True

            class_window.bind('<Return>', lambda self: save_new_character())

        canvas2.tag_bind('Secateurs', '<Button-1>', lambda self: entry_name('Secateurs'))
        canvas2.tag_bind('Reactive', '<Button-1>', lambda self: entry_name('Reactive'))
        canvas2.tag_bind('Celestial', '<Button-1>', lambda self: entry_name('Celestial'))
        class_window.bind('<Escape>', lambda self: class_window.destroy())
        class_window.mainloop()
        global stop
        stop = True

    def continue_game():
        try:
            json.load(open('Data/data_file.json'))
        except FileNotFoundError:
            messagebox.showerror('Error!', 'Сохраненные данные не найдены')
        else:
            if path.isfile('Data/Game_testing.pyw'):
                subprocess.Popen([sys.executable, 'Data/Game_testing.pyw'])
            else:
                subprocess.Popen('Data/Game_testing.exe')
            window.overrideredirect(False)
            window.iconify()
            global stop
            stop = True

    def program_exit():
        global stop
        stop = True
        sys.exit()

    canvas.create_image(400, 120, image=bg)
    canvas.create_line(0, 280, 800, 280, fill='white', width=40)

    Button(window,
           text='New Game',
           command=start_new_game,
           bg='white',
           fg='black',
           relief='flat',
           font='Plateia 10',
           ).place(x=450, y=260)
    Button(window,
           text='Continue',
           bg='white',
           fg='black',
           relief='flat',
           command=continue_game,
           font='Plateia 10'
           ).place(x=580, y=260)

    Button(window,
           text='Exit',
           bg='white',
           fg='black',
           relief='flat',
           font='Plateia 10',
           command=program_exit
           ).place(x=700, y=260)

    rnd = randint(20, 40)
    if rnd % 2 != 0:
        rnd += 1
    points = []
    for i in range(rnd):
        if i % 2 != 0:
            points.append(randint(30, 210))
        else:
            points.append(randint(100, 250))
    canvas.create_polygon(points, fill='grey', tag='pol')
    canvas.update()

    canvas.create_text(150, 90, text='Pantheum', font='Plateia 25', fill='white')
    canvas.create_text(65, 115, text='VER.06', font='Plateia 10', fill='white', activefill='red')


if __name__ == '__main__':
    window.mainloop()
